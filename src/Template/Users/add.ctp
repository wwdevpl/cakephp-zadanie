<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Logowanie</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Logowanie</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-body">

          <div class="users form">
            <?= $this->Form->create($user) ?>
            <fieldset>
              <legend><?= __('Add User') ?></legend>
              <?= $this->Form->control('username') ?>
              <?= $this->Form->control('password') ?>
              <?= $this->Form->control('role', ['options' => ['admin' => 'Admin', 'author' => 'Author']]) ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')); ?>
            <?= $this->Form->end() ?>
          </div>

        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
