<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Logowanie</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Logowanie</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-body">

          <?= $this->Flash->render() ?>
          <?= $this->Form->create() ?>
            <fieldset>
              <legend><?= __('Please enter your username and password') ?></legend>
              <?= $this->Form->control('username') ?>
              <?= $this->Form->control('password') ?>
          </fieldset>
          <?= $this->Form->button(__('Login')); ?>
          <?= $this->Form->end() ?>

        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
