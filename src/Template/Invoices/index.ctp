<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Faktury</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Faktury</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Numer faktury</th>
                  <th>Szczegóły</th>
                  <th>Kwota faktury</th>
                  <th>Nazwa firmy</th>
                  <th>Adres</th>
                  <th>Adres e-mail</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                  <th>Numer faktury</th>
                  <th>Szczegóły</th>
                  <th>Kwota faktury</th>
                  <th>Nazwa firmy</th>
                  <th>Adres</th>
                  <th>Adres e-mail</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
