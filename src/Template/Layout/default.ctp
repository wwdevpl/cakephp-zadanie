<!DOCTYPE html>
<html>
<head>
  <?= $this->Html->charset() ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Zadanie</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <?= $this->Html->css('/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
  <!-- Font Awesome -->
  <?= $this->Html->css('/bower_components/font-awesome/css/font-awesome.min.css') ?>
  <!-- Ionicons -->
  <?= $this->Html->css('/bower_components/Ionicons/css/ionicons.min.css') ?>
  <!-- Theme style -->
  <?= $this->Html->css('/dist/css/AdminLTE.min.css') ?>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <?= $this->Html->css('/dist/css/skins/_all-skins.min.css') ?>
  <!-- Morris chart -->
  <?= $this->Html->css('/bower_components/morris.js/morris.css') ?>
  <!-- jvectormap -->
  <?= $this->Html->css('/bower_components/jvectormap/jquery-jvectormap.css') ?>
  <!-- Date Picker -->
  <?= $this->Html->css('/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>
  <!-- Daterange picker -->
  <?= $this->Html->css('/bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <?= $this->Html->css('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>
  <!-- DataTables -->
  <?= $this->Html->css('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>

  <?php echo $this->fetch('css'); ?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $this->Url->build('/'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <?php if ($this->request->Session()->read('Auth.User')): ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">Wojciech Walęczewski</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">

                <p>
                  Wojciech Walęczewski - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?php echo $this->Url->build('/users/logout'); ?>" class="btn btn-default btn-flat">Wyloguj</a>
                </div>
              </li>
            </ul>
          </li>
        <?php endif; ?>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Nawigacja</li>
        <li><a href="<?php echo $this->Url->build('/dashboard'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="<?php echo $this->Url->build('/invoices'); ?>"><i class="fa fa-table"></i> <span>Faktury</span></a></li>
        <li><a href="<?php echo $this->Url->build('/settings'); ?>"><i class="fa fa-cogs"></i> <span>Ustawienia</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <?= $this->fetch('content') ?>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
</div>

  <!-- jQuery 3 -->
  <?php echo $this->Html->script('/bower_components/jquery/dist/jquery.min.js'); ?>
  <!-- jQuery UI 1.11.4 -->
  <?php echo $this->Html->script('/bower_components/jquery-ui/jquery-ui.min.js'); ?>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <?php echo $this->Html->script('/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>
  <!-- Morris.js charts -->
  <?php echo $this->Html->script('/bower_components/raphael/raphael.min.js'); ?>
  <?php echo $this->Html->script('/bower_components/morris.js/morris.min.js'); ?>
  <!-- Sparkline -->
  <?php echo $this->Html->script('/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js'); ?>
  <!-- jvectormap -->
  <?php echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>
  <?php echo $this->Html->script('/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>
  <!-- jQuery Knob Chart -->
  <?php echo $this->Html->script('/bower_components/jquery-knob/dist/jquery.knob.min.js'); ?>
  <!-- daterangepicker -->
  <?php echo $this->Html->script('/bower_components/moment/min/moment.min.js'); ?>
  <?php echo $this->Html->script('/bower_components/bootstrap-daterangepicker/daterangepicker.js'); ?>
  <!-- datepicker -->
  <?php echo $this->Html->script('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>
  <!-- Bootstrap WYSIHTML5 -->
  <?php echo $this->Html->script('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>
  <!-- Slimscroll -->
  <?php echo $this->Html->script('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
  <!-- FastClick -->
  <?php echo $this->Html->script('/bower_components/fastclick/lib/fastclick.js'); ?>
  <!-- AdminLTE App -->
  <?php echo $this->Html->script('/dist/js/adminlte.min.js'); ?>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <?php echo $this->Html->script('/dist/js/pages/dashboard.js'); ?>
  <!-- AdminLTE for demo purposes -->
  <?php echo $this->Html->script('/dist/js/demo.js'); ?>
  <!-- DataTables -->
  <?php echo $this->Html->script('/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>
  <?php echo $this->Html->script('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>

  <?php echo $this->fetch('script'); ?>

  <script>
  $(function () {
    $('#example1').DataTable({
        "ajax": '<?php echo $this->Url->build('/api/invoices'); ?>'
    });
  })
  </script>

  </body>
  </html>
