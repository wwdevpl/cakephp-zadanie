<?php
namespace App\Controller;

use App\Controller\AppController;

class ApiController extends AppController
{
    public function invoices()
    {
      $this->viewBuilder()->className('ajax');

      $this->loadModel('Invoices');

      $this->RequestHandler->renderAs($this, 'json');

      $invoices = $this->Invoices->find();
      $this->set(compact('invoices'));
    }
}
