<?php
use Migrations\AbstractMigration;

class CreateInvoices extends AbstractMigration
{
    public function change()
    {
      $table = $this->table('invoices');
      $table->addColumn('number', 'string', [
          'default' => null,
          'limit' => 255,
          'null' => false,
      ]);
      $table->addColumn('details', 'text', [
          'default' => null,
          'null' => false,
      ]);
      $table->addColumn('amount', 'float', [
          'default' => null,
          'null' => false,
      ]);
      $table->addColumn('company_name', 'string', [
          'default' => null,
          'limit' => 255,
          'null' => false,
      ]);
      $table->addColumn('address', 'text', [
          'default' => null,
          'null' => false,
      ]);
      $table->addColumn('email', 'string', [
          'default' => null,
          'limit' => 255,
          'null' => false,
      ]);
      $table->create();
    }
}
