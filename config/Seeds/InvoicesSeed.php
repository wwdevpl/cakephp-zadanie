<?php
use Migrations\AbstractSeed;

class InvoicesSeed extends AbstractSeed
{
    public function run()
    {
        $data = [
          [
            'number' => '200/FS/2018',
            'details' => 'Test',
            'amount' => '100',
            'company_name' => 'Przykładowa',
            'address' => 'Szkolna 17, Poznan',
            'email' => 'wojciech.waleczewski@gmail.com'
          ],
          [
            'number' => '354/FS/2018',
            'details' => 'Test2',
            'amount' => '1200.82',
            'company_name' => 'Firma 2 sp. z oo',
            'address' => 'Grunwaldzka, Poznan',
            'email' => 'jakas@domena.com'
          ]
        ];

        $table = $this->table('invoices');
        $table->insert($data)->save();
    }
}
