<?php
use Migrations\AbstractSeed;

class UsersSeed extends AbstractSeed
{
    public function run()
    {
        $data = [
          [
            'username' => 'demo',
            'password' => '$2y$10$BKaJ2WP0VTyf3zitKibUb.BmAZ22l5zCxsTHf7LN5pWBPXxCkvt0i',
            'role' => 'admin',
            'created' => date('Y-m-d H:m:i'),
            'modified' => date('Y-m-d H:m:i')
          ]
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
